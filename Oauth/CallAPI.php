<?php

namespace App\_lib\Oauth;

// guzzleの呼び出し
use Illuminate\Support\Facades\Http;

class CallAPI
{

    /**
     * Token認証を実施
     *
     * @param Array $state
     * @return void
     */
    public static function redirect(Array $state)
    {
        // ランダム文字列生成
        $request->session()->put('state', $st = Str::random(40));
        // 送信パラメータ
        $query = http_build_query([
            'client_id'     => $state['client_id'],
            'redirect_uri'  => $state['redirect_uri'],
            'response_type' => $state['response_type'],
            'scope'         => $state['scope'],
            'state'         => $st,
        ]);

        return redirect('/oauth/authorize?' . $query);
    }

    public static function getAuthorize()
    {

    }

    public static function deleteAuthorize()
    {

    }

    /**
     * Clientを作成
     * API Path :/oauth/clients
     * METHOD   :POST
     *
     * @param Array $state
     * @return void
     */
    public static function createClient(Array $state)
    {
        // 送信パラメータ
        $query = http_build_query([
            'name'      => $state['name'],
            'redirect'  => $state['redirect'],
        ]);

        // APIをコール
        $result = Http::post(
            '/oauth/clients',
            $query
        );

        return $result;
    }

    /**
     * Clientリストを取得
     * API Path :/oauth/clients
     * METHOD   :POST
     *
     * @return void
     */
    public static function getClient()
    {
        $resutl = Http::get('/oauth/clients');
        return $result;
    }

    public static function deleteClient(Array $state)
    {
        // APIをコール
        $result = Http::request(
            'DELETE',
            '/oauth/clients/' . $state['clientId']
        );

        return $result;
    }

    public static function updateClient(Array $state)
    {
        // 送信パラメータ
        $query = http_build_query([
            'name'      => $state['name'],
            'redirect'  => $state['redirect'],
        ]);

        // APIをコール
        $result = Http::request(
            'PUT',
            '/oauth/clients/' . $state['clientId'],
            $query
        );

        return $result;
    }

    public static function createPersonalAccessTokens()
    {

    }

    public static function getPersonalAccessTokens()
    {

    }

    public static function deletePersonalAccessTokens()
    {

    }

    public static function getScopes()
    {

    }
    
    public static function createAccessToken()
    {

    }

    public static function getAccessTokens()
    {

    }

    public static function deleteAccessTokens()
    {

    }
    
}
