<?php

namespace App\_lib\Helper;

use App\_lib\Fido\Fido;

class FidoHelper
{
    private $rpid = '';
    private $rpname = '';

    public function __constructor(
        string $rpid = null, string $rpname = null
    ) {
        $this->rpid = ($rpid)? $rpid : '';
        $this->rpname = ($rpname)? $rpname : '';
    }

    public function createCredentialOption(Array $user)
    {
        // Fidoヘルパー関数取得
        $cr = Fido::CredentialRepository();
        
        $clientCredentialOption = $cr
            // RP情報登録
            ->setRP([
                'id' => $this->rpid,
                'name' => $this->rpname])
            // クライアントからの情報を登録
            ->setClientRequest($user)
            // clientCredentailOptionを作成
            ->buildClientCredentialOptionToCreate()
            // FIDO-U2Fサポートを追加
            ->addEC2KeyType()
            // clientCredentialOptionを取得
            ->getClientCredentialOption();
    }

}
