<?php

namespace App\_lib\Helper;

use Illuminate\Support\Facades\Auth;
use App\Config;
use Carbon\Carbon;

class Token
{

    /**
     * トークンを新規に取得
     *
     * @return string
     */
    public static function getToken(): string
    {
        $user = Auth::user();
        /*
        $_token = self::checkExpires($user->tokens()->get()->toArray());
        if ($_token === '') {
            $token = $user->createToken('token_for_' . $user->id)->accessToken;
            return $token;
        } else {
            return $_token;
        } */
        $hoge = $user->tokens();

        $token = $user->createToken('token_for_' . $user->id)->accessToken;
        return $token;
    }

    /**
     * 取り出したトークン情報の作成日時が
     * 設定された有効期限内の場合は返す
     * 一つも存在しない場合は[""]を返す
     *
     * @param Array $tokens
     * @return string
     */
    private static function checkExpires(Array $tokens): string
    {
        $ex = config('app.expire_time');

        foreach ($tokens as $key => $val) {
            $ch_time = new Carbon($tokens[$key]['created_at']);
            $ex_time = new Carbon();
            if ($ch_time->diffInSeconds($ex_time) < $ex) {
                return $tokens[$key]['id'];
            }
        }
        return '';
    }

}
